package com.epam.tat.module4.page;

import com.epam.tat.module4.utils.configProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleCloudPricingCalcPage extends AbstractPage {

    @FindBy(css = "#cloud-site > devsite-iframe > iframe")
    private WebElement globalIframe;

    public GoogleCloudPricingCalcPage(WebDriver driver) {
        super(driver);
        new WebDriverWait(driver, TIMEOUT)
                .until(ExpectedConditions.or(ExpectedConditions
                                .elementToBeClickable(By.xpath("//*[@class='AeBiU-LgbsSe AeBiU-LgbsSe-OWXEXe-Bz112c-M1Soyc AeBiU-LgbsSe-OWXEXe-dgl2Hf VVEJ3d']")),
                        ExpectedConditions.presenceOfElementLocated(By.cssSelector("#cloud-site > devsite-iframe > iframe"))
                ));
        try {
            if (driver.findElement(By.xpath("//*[@class='AeBiU-LgbsSe AeBiU-LgbsSe-OWXEXe-Bz112c-M1Soyc AeBiU-LgbsSe-OWXEXe-dgl2Hf VVEJ3d']")) != null) {
                driver.get(configProperties.getProperty().getProperty("CalculatorLegacyURL"));
            }
        } catch (NoSuchElementException e) {
        }
    }

    public GoogleCalculatorIframePage switchToCalculatorIframe() {
        globalIframe.click();
        driver.switchTo().frame(globalIframe);
        WebElement calculatorIframe = driver.findElement(By.id("myFrame"));
        driver.switchTo().frame(calculatorIframe);
        return new GoogleCalculatorIframePage(driver);
    }
}
