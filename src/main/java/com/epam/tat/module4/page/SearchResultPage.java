package com.epam.tat.module4.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.epam.tat.module4.waits.CustomConditions;

import java.util.List;

public class SearchResultPage extends AbstractPage {
    private String title;
    @FindBy(xpath = "//*[@track-metadata-module=\"search-results\"]")
    private List<WebElement> generalSearchResult;

    SearchResultPage(WebDriver driver, String title) {
        super(driver);
        new WebDriverWait(driver, TIMEOUT)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@track-metadata-module=\"search-results\"]")));
        this.title = title;
    }

    public GoogleCloudPricingCalcPage openGoogleCalculatorPage() {
        findNeededLinkUsingKeywordsFromTitle().click();
        new WebDriverWait(driver, TIMEOUT).until(CustomConditions.JSpageReady());
        return new GoogleCloudPricingCalcPage(driver);
    }

    private WebElement findNeededLinkUsingKeywordsFromTitle() {
        int maxKeywordCount = 0;
        String[] keywords = title.split(" ");
        WebElement elementToReturn = generalSearchResult.get(0);
        for (WebElement element : generalSearchResult) {
            String text = element.getText();
            int keywordCout = 0;
            for (String word : keywords) {
                if (text.contains(word)) keywordCout++;
            }
            if (keywordCout > maxKeywordCount) {
                maxKeywordCount = keywordCout;
                elementToReturn = element;
            }
        }
        return elementToReturn;
    }
}
