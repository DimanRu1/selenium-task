package com.epam.tat.module4.page;

import com.epam.tat.module4.utils.ParseUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Locale;

public class GoogleCalculatorIframePage extends AbstractPage {

    @FindBy(id = "input_100")
    private WebElement numberOfInstances;
    @FindBy(id = "select_113")
    private WebElement operatingSystemSelect;
    @FindBy(id = "select_117")
    private WebElement provisioningModelSelect;
    @FindBy(id = "select_123")
    private WebElement machineFamilySelect;
    @FindBy(id = "select_125")
    private WebElement seriesSelect;
    @FindBy(id = "select_127")
    private WebElement machineSelect;
    @FindBy(xpath = "//*[@ng-model='listingCtrl.computeServer.addGPUs']")
    private WebElement checkBoxGPUs;
    @FindBy(xpath = "//*[@ng-model='listingCtrl.computeServer.gpuType']")
    private WebElement gpuTypeSelect;
    @FindBy(xpath = "//*[@ng-model='listingCtrl.computeServer.gpuCount']")
    private WebElement gpuNumberSelect;
    @FindBy(id = "select_469")
    private WebElement localStorageSelect;
    @FindBy(id = "select_133")
    private WebElement dataCenterSelect;
    @FindBy(id = "select_140")
    private WebElement commitedUsageSelect;

    protected GoogleCalculatorIframePage(WebDriver driver) {
        super(driver);
    }

    public GoogleCalculatorIframePage setNumberOfInstances(int number) {
        numberOfInstances.click();
        numberOfInstances.sendKeys(Integer.toString(number));
        return this;
    }

    public GoogleCalculatorIframePage selectOperatingSystem(String witchToSelect) {
        return HandleMDSelect(operatingSystemSelect, witchToSelect, "md-text", "select_container_114");
    }

    public GoogleCalculatorIframePage selectProvisioningModel(String witchToSelect) {
        return HandleMDSelect(provisioningModelSelect, witchToSelect, "md-text", "select_container_118");
    }

    public GoogleCalculatorIframePage selectMachineFamily(String witchToSelect) {
        return HandleMDSelect(machineFamilySelect, witchToSelect, "md-text", "select_container_124");
    }

    public GoogleCalculatorIframePage selectSeries(String witchToSelect) {
        return HandleMDSelect(seriesSelect, witchToSelect, "md-text ng-binding", "select_container_126");
    }

    public GoogleCalculatorIframePage selectMachineType(String witchToSelect) {
        return HandleMDSelect(machineSelect, witchToSelect, "md-text ng-binding", "select_container_128");
    }

    public GoogleCalculatorIframePage addGPUs(String typeOfGpu, String numberOfGpus) {
        checkBoxGPUs.click();
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@ng-model='listingCtrl.computeServer.gpuType']")));
        if (typeOfGpu == null)
            return HandleMDSelect(gpuNumberSelect, numberOfGpus, "md-text ng-binding", calculateContainerID(gpuNumberSelect.getAttribute("id")));
        if (numberOfGpus == null)
            return HandleMDSelect(gpuTypeSelect, typeOfGpu, "md-text ng-binding", calculateContainerID(gpuTypeSelect.getAttribute("id")));
        HandleMDSelect(gpuTypeSelect, typeOfGpu, "md-text ng-binding", calculateContainerID(gpuTypeSelect.getAttribute("id")));
        return HandleMDSelect(gpuNumberSelect, numberOfGpus, "md-text ng-binding", calculateContainerID(gpuNumberSelect.getAttribute("id")));
    }

    public GoogleCalculatorIframePage selectLocalSSD(String witchToSelect) {
        return HandleMDSelect(localStorageSelect, witchToSelect, "md-text ng-binding", "select_container_470");
    }

    public GoogleCalculatorIframePage selectDataCenter(String witchToSelect) {
        return HandleMDSelect(dataCenterSelect, witchToSelect, "md-text ng-binding", "select_container_134");
    }

    public GoogleCalculatorIframePage selectCommitedUsage(String witchToSelect) {
        return HandleMDSelect(commitedUsageSelect, witchToSelect, "md-text", "select_container_141");
    }

    public GoogleCalculatorIframePage selectService(String option) {
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//*[@class= 'md-tab ']")));
        driver.findElement(By.xpath("//span[@class='ng-binding' and text()='" + option + "']/../..")).click();
        return this;
    }

    private String calculateContainerID(String elementSelectID) {
        String numericPart = elementSelectID.replaceAll("[^0-9]", "");
        int resultNumber = Integer.parseInt(numericPart);
        resultNumber++;
        return "select_container_" + resultNumber;
    }

    private GoogleCalculatorIframePage HandleMDSelect(WebElement elementToClick, String value, String elementClass, String selectContainerID) {
        elementToClick.click();
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.id(selectContainerID)));
        String elementXpath = "//*[@id='" + selectContainerID + "']//div[@class='" + elementClass + "' and contains(text(), '" + value + "')]";
        driver.findElement(By.xpath(elementXpath)).click();
        return this;
    }

    public Estimate addToEstimate() {
        WebElement estimateButton = driver.findElement(By.xpath("//*[@ng-click='listingCtrl.addComputeServer(ComputeEngineForm);']"));
        new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.elementToBeClickable(estimateButton));
        estimateButton.click();
        return new Estimate();
    }

    public class Estimate {
        private Estimate() {
        }

        public double getTotalCost() {
            WebElement cost = new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='md-toolbar-tools layout-align-space-between-center layout-row']//*[@class='md-flex ng-binding ng-scope']")));
            String text = cost.getText().trim();
            String numericString = text.replaceAll("[^\\d.]", "");

            return ParseUtil.parseStringToDouble(numericString, Locale.US);
        }

        public void sendOnEmail(String email) {
            WebElement emailButton = new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.id("Email Estimate")));
            emailButton.click();
            WebElement emailInputBox = new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@ng-model='emailQuote.user.email']")));
            emailInputBox.sendKeys(email);
            WebElement sendButton = new WebDriverWait(driver, TIMEOUT).until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Send Email')]")));
            sendButton.click();
        }
    }
}
