package com.epam.tat.module4.driver;

import com.epam.tat.module4.utils.TabManager;
import com.epam.tat.module4.utils.configProperties;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;

public class DriverSingleton {
    private static WebDriver driver;
    private static TabManager tabManager;

    private DriverSingleton() {
    }

    public static WebDriver getDriver() {
        if (null == driver) {
            switch (configProperties.getProperty().getProperty("BrowserType")) {
                case "firefox": {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    return driver;
                }
                case "chrome": {
                    WebDriverManager.chromedriver().setup();
                    ChromeOptions ops = new ChromeOptions();
                    ops.addExtensions(new File("./extentions/Adblock Plus.crx"));
                    driver = new ChromeDriver(ops);
                    tabManager = new TabManager(driver);
                    new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.numberOfWindowsToBe(2));
                    tabManager.switchToTab("Adblock");
                    tabManager.closeCurrentTab();
                    tabManager.switchToLastTab();
                    return driver;
                }
                default: {
                    WebDriverManager.chromedriver().setup();
                    ChromeOptions ops = new ChromeOptions();
                    ops.addExtensions(new File("./extentions/Adblock Plus.crx"));
                    driver = new ChromeDriver(ops);
                    tabManager = new TabManager(driver);
                    new WebDriverWait(driver, Duration.ofSeconds(10)).until(ExpectedConditions.numberOfWindowsToBe(2));
                    tabManager.switchToTab("Adblock");
                    tabManager.closeCurrentTab();
                    tabManager.switchToLastTab();
                }
            }
        }
        return driver;
    }
}
