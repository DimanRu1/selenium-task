package com.epam.tat.module4.utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class ParseUtil {
    private ParseUtil(){}
    public static double parseStringToDouble(String numericString, Locale locale)
    {
        double numericValue = 0;
        try {
            numericValue = NumberFormat.getInstance(locale).parse(numericString).doubleValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numericValue;
    }
}
